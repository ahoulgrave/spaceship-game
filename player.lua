player = {}
player.w = 80
player.h = 24
player.x = 20
player.y = love.graphics.getHeight()/2 - player.h/2
player.maxSpeed = 300
player.speed = 0
player.xSpeed = 0
player.ySpeed = 0
player.acceleration = 2
player.friction = 20
player.bullets = {}
player.cooldown = 20
player.points = 0
player.level_points = 0
local fire_sound = love.audio.newSource("assets/bullet.wav", "stream")

player.fire = function()
  if (player.cooldown > 0) then
    return
  end
  
  player.cooldown = 20
  
  local trail = love.graphics.newCanvas(2.5, 2.5)
  love.graphics.setCanvas(trail)
  love.graphics.setColor(255, 255, 0, .8)
  love.graphics.circle("fill", 2.5, 2.5, 2.5)
  love.graphics.setCanvas()
  
  local ps = love.graphics.newParticleSystem(trail, 10)
  ps:setParticleLifetime(10)
  ps:setEmitterLifetime(1000)
  ps:setEmissionRate(2000)
  ps:setSpeed(200, 500)
  ps:setLinearAcceleration(0, -10, 0, 10)
  
  fire_sound:play()
  
  table.insert(player.bullets, {
    x = player.x + 85,
    y = player.y + 28,
    w = 20,
    h = 10,
    speed = 500,
    ps = ps
  })
end