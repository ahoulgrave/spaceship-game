-- fire sounds: https://opengameart.org/content/laser-fire
-- music: https://opengameart.org/art-search-advanced?keys=&field_art_type_tid%5B%5D=12&sort_by=count&sort_order=DESC
-- explosion: https://opengameart.org/content/explosion
-- boom sound: https://opengameart.org/content/atari-booms
-- enemy explosion sound: https://opengameart.org/content/retro-sounds-0

require("menu")
require("game")
require("game_over")
require("level")
require("ending")

god_mode = false

function love.load()
  state = "menu"
  
  fonts = {}
  
  level = 0 -- starting level
  
  music = {}
  music.menu = love.audio.newSource("assets/menu.wav", "static")
  music.menu:setLooping(true)
  music.menu:play()
  
  music.game = love.audio.newSource("assets/game.wav", "static")
  music.game:setLooping(true)
  
  music.game_over = love.audio.newSource("assets/game_over.wav", "static")
  music.game_over:setLooping(true)
  
  menu_load()
  game_load()
  level_load()
  game_over_load()
  ending_load()
  
  if god_mode then
    god_mode_image = love.graphics.newImage("assets/god_mode.png")
  end
end

function love.update(dt)
  if state == "menu" then
    menu_update(dt)
  elseif state == "game" then
    game_update(dt)
  elseif state == "level" then
    level_update(dt)
  elseif state == "ending" then
    ending_update(dt)
  end
end

function love.draw()
  if state == "menu" then
    menu_draw()
  elseif state == "game" then
    game_draw()
  elseif state == "level" then
    level_draw()
  elseif state == "game_over" then
    game_over_draw()
  elseif state == "ending" then
    ending_draw()
  end
  
  if god_mode then
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(god_mode_image, 10, love.graphics.getHeight() - god_mode_image:getHeight() - 10)
  end
end

function love.keypressed(key)
  if state == "menu" then
    menu_keypressed(key)
  elseif state == "game" then
    game_keypressed(key)
  elseif state == "game_over" then
    game_over_keypressed(key)
  end
end