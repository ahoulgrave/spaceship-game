local levels = {}

levels[0] = {
  enemies = {
    basic = 2,
    crosser = 2
  },
  spawn_type = function ()
    rand = math.random(0, 100)
    if rand >= 50 then
      return "crosser"
    else
      return "basic"
    end
  end,
  enemy_spawn_rate = 1,
  points = 10,
}

levels[1] = {
  enemies = {
    basic = 10
  },
  spawn_type = function ()
    return "basic"
  end,
  enemy_spawn_rate = 2,
  points = 10,
}

levels[2] = {
  enemies = {
    basic = 20
  },
  spawn_type = function ()
    return "basic"
  end,
  enemy_spawn_rate = 3,
  points = 10,
}

return levels