function game_over_load()
  fonts.game_over = love.graphics.newFont("assets/Connection.otf", 60)
end

function game_over_draw()
  love.graphics.setColor(255, 255, 255)
  love.graphics.setFont(fonts.game_over)
  local fontWidth = fonts.game_over:getWidth("Game over")
  local fontHeight = fonts.game_over:getHeight("Game over")
  love.graphics.print("Game over", (love.graphics.getWidth() / 2) - (fontWidth/2), (love.graphics.getHeight()/2) - fontHeight/2)
end

function game_over_keypressed(key)
  if key == "space" then
    music.game_over:stop()
    music.menu:play()
    state = "menu"
  end
end