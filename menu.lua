local buttons = {}
local selected_button = 1

function menu_load()
  fonts.menu_title  = love.graphics.newFont("assets/Connection.otf", 70)
  fonts.menu_button = love.graphics.newFont("assets/Connection.otf", 40)
  
  table.insert(buttons, {text="Start Game", callback=function() 
      state = "level"
      music.menu:stop()
      music.game:play()
    end})
  
  -- quit button
  table.insert(buttons, {
    text="Quit", 
    callback=function()
      love.event.quit() 
    end
  })
end

function menu_update(dt)
end

function menu_draw()
  -- draw title
  love.graphics.setColor(255, 255, 255)-- white
  love.graphics.setFont(fonts.menu_title)
  local titleWidth = fonts.menu_title:getWidth("Spaceship Game")
  love.graphics.print("Spaceship Game", (love.graphics.getWidth()/2) - titleWidth/2, 120)
  
  -- draw buttons
  love.graphics.setFont(fonts.menu_button)
  for i, button in ipairs(buttons) do
    local buttonWidth = fonts.menu_button:getWidth(button.text)
    if selected_button == i then
      love.graphics.setColor(255, 255, 0)-- yellow
    else
      love.graphics.setColor(255, 255, 255)-- white
    end
    love.graphics.print(button.text, (love.graphics.getWidth()/2) - buttonWidth/2, 250 + (60 * i))
  end
end

function menu_keypressed(key)
  if key == "down" then
    local nextButton = selected_button + 1
    if nextButton > #buttons then
      selected_button = 1
    else
      selected_button = nextButton
    end
  end
  
  if key == "up" then
    local nextButton = selected_button - 1
    if nextButton == 0 then
      selected_button = #buttons
    else
      selected_button = nextButton
    end
  end
  
  if key == "space" then
    buttons[selected_button].callback()
  end
end