enemy_types = {
  basic = {
    name = 'basic',
    speed = -200,
    points = 1,
    getX = function (x)
      return x
    end,
    getY = function (y)
      return y
    end,
  },
  crosser = {
    name = 'crosser',
    speed = -200,
    points = 2,
    getX = function (x)
      return math.random(x/2, x)
    end,
    getY = function (y)
      -- this enemy starts from the bottom or the top. the -30 is enemy height
      return math.random(0, 1) == 1 and love.graphics.getHeight() or -30
    end,
  }
}

enemy_controller = {}
enemy_controller.enemies = {}
enemy_controller.spawn_rate = {}
enemy_controller.do_spawn = 0
enemy_controller.bullets = {}
enemy_controller.explosions = {}
enemy_controller.enemies_killed = {
  basic = 0,
  crosser = 0,
}

enemy_controller.hard_fire_rate = 100
enemy_controller.fire_rate = enemy_controller.hard_fire_rate
enemy_controller.do_fire = enemy_controller.fire_rate

local fire_sound = love.audio.newSource("assets/enemy_bullet.wav", "stream")

enemy_controller.spawn = function(x, y, enemyType)
  -- x = love.graphics.getWidth()
  table.insert(enemy_controller.enemies, {
    x = enemy_types[enemyType].getX(x),
    y = enemy_types[enemyType].getY(y),
    w = 60,
    h = 30,
    type = enemy_types[enemyType]
  })
end

enemy_controller.fire = function(enemy)
  fire_sound:play()
  table.insert(enemy_controller.bullets, {
    x = enemy.x,
    y = enemy.y + 20,
    h = 10,
    w = 10,
    speed = -500
  })
end