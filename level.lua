local wait = 3 -- duration of level screen

function level_load()
  fonts.level_screen = love.graphics.newFont("assets/Connection.otf", 80)
end

function level_update(dt)
  wait = wait - dt
  
  if wait <= 0 then
    state = "game"
    wait = 3
  end
end

function level_draw()
  love.graphics.setColor(255,255,255)
  love.graphics.setFont(fonts.level_screen)
  local text = "Stage " .. (level + 1)
  
  local textW = fonts.level_screen:getWidth(text)
  local textH = fonts.level_screen:getHeight(text)
  
  love.graphics.print(text, (love.graphics.getWidth()/2) - (textW/2), (love.graphics.getHeight()/2) - (textH/2))
end