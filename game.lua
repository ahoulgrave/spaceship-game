require("player")
require("enemy")

local levels = require("data/levels")

local player_image
local bullet_image
local enemy_bullet_image
local enemy_image
local started
local startTime = 1-- change to 5. is in 1 for debug only
local ps_bg
local dieing = false
local dieing_waiting_time = 3
local dieing_time = dieing_waiting_time
local animation
local explosion_sound
local enemy_explosion_sound, enemy_explosion_animation
local level_won_time_max = 1.5
local level_won_time = level_won_time_max
local level_won = false -- if true, the player already won the level. he won't die
local level_won_back_time = 1 -- to handle winning level animation

function game_load()
  started = startTime -- time before starting
  player_image = love.graphics.newImage("assets/player.png")
  bullet_image = love.graphics.newImage("assets/bullet.png")
  enemy_image = love.graphics.newImage("assets/enemy.png")
  enemy_bullet_image = love.graphics.newImage("assets/enemy_bullet.png")
  
  fonts.start_game = love.graphics.newFont("assets/Connection.otf", 30)
  
  -- background particles
  local canvas = love.graphics.newCanvas(2, 2)
  love.graphics.setCanvas(canvas)
  love.graphics.setColor(255,255,255)
  love.graphics.circle("fill", 2, 2, 2)
  love.graphics.setCanvas()
  
  ps_bg = love.graphics.newParticleSystem(canvas, 1000)
  ps_bg:setParticleLifetime(10)
  ps_bg:setSpeed(-500, -50)
  ps_bg:setSizes(1,1.5)
  
  -- explosion animation
  explosion_sound = love.audio.newSource("assets/explosion.wav", "static")
  animation = newAnimation(love.graphics.newImage("assets/explosion.png"), 64, 64, 1)
  
  enemy_explosion_sound = love.audio.newSource("assets/enemy_explosion.wav", "static")
  
  -- enemy explosion
  enemy_explosion_animation = love.graphics.newImage("assets/explosion.png")
end

function game_update(dt)
  ps_bg:setPosition(love.graphics.getWidth(), math.random(0, love.graphics.getHeight()))
  ps_bg:emit(1)
  ps_bg:update(dt)
  
  enemy_controller.spawn_rate = 1/levels[level].enemy_spawn_rate
  
  if started > 0 then
    started = started - dt
    return
  end
  
  -- handle enemies
  for i, enemy in ipairs(enemy_controller.enemies) do
    enemy.x = enemy.x + enemy.type.speed * dt
    
    if enemy.x < -enemy.w then
      table.remove(enemy_controller.enemies, i)
    end
  end
  
  -- spawn enemies
  if enemy_controller.do_spawn > 0 then
    enemy_controller.do_spawn = enemy_controller.do_spawn - dt
  end
  
  if enemy_controller.do_spawn <= 0 then
    enemy_controller.do_spawn = enemy_controller.spawn_rate
    enemy_controller.spawn(love.graphics.getWidth(), math.random(0, love.graphics.getHeight() - 20), levels[level].spawn_type())
  end
  
  -- move enemy
  for i, enemy in ipairs(enemy_controller.enemies) do
    move_enemy(enemy, dt)
  end
  
  -- move enemy fire
  for i, bullet in ipairs(enemy_controller.bullets) do
    bullet.x = bullet.x + bullet.speed * dt
    
    -- handle collision
    if bullet.x < player.x + player.w and bullet.x > player.x and bullet.y + bullet.h > player.y and bullet.y < player.y + player.h and not dieing and not level_won and not god_mode then
      -- we got hit!!
      explosion_sound:play()
      table.remove(enemy_controller.bullets, i)
      dieing = true
      return
    end
  end
  
  -- handle collision
  for i, bullet in ipairs(player.bullets) do
    for j, enemy in ipairs(enemy_controller.enemies) do
      if bullet.x + bullet.w > enemy.x and bullet.x < enemy.x + enemy.w and bullet.y + bullet.h > enemy.y and bullet.y < enemy.y + enemy.h then
        -- houston we got a collision
        enemy_explosion_sound:play()
        table.insert(enemy_controller.explosions, {
          animation = newAnimation(enemy_explosion_animation, 64, 64, 1),
          x = enemy.x + (enemy.w/2),
          y = enemy.y + (enemy.h/2),
          spriteNum = {}
        })
        table.remove(player.bullets, i)
        table.remove(enemy_controller.enemies, j)
        enemy_controller.enemies_killed[enemy.type.name] = enemy_controller.enemies_killed[enemy.type.name] + 1
        player.points = player.points + enemy.type.points
        player.level_points = player.level_points + enemy.type.points
      end
    end
  end
  
  -- enemy fire rate
  if enemy_controller.do_fire > 0 and not level_won then
    enemy_controller.do_fire = enemy_controller.do_fire - 1
  end
  
  if enemy_controller.do_fire <= 0 and #enemy_controller.enemies > 0 then
    enemy_controller.fire_rate = enemy_controller.hard_fire_rate / #enemy_controller.enemies
    enemy_controller.do_fire = enemy_controller.fire_rate
    
    enemy_controller.fire(enemy_controller.enemies[math.random(#enemy_controller.enemies)])
  end 
  
  -- handle bullets
  for i, bullet in ipairs(player.bullets) do
    bullet.x = bullet.x + bullet.speed * dt
    bullet.ps:setPosition(bullet.x , bullet.y)
    bullet.ps:emit(1)
    bullet.ps:update(dt)
    if bullet.x > love.graphics.getWidth() then
      table.remove(player.bullets, i)
    end
  end
  
  for _, explosion in pairs(enemy_controller.explosions) do
    explosion.animation.currentTime = explosion.animation.currentTime + dt
  end
  
  -- handle user input
  if not dieing and not level_won then
    if love.keyboard.isDown("down") and player.y < love.graphics.getHeight() - player.h then
      if player.ySpeed < player.maxSpeed then
        player.ySpeed = player.maxSpeed
      end
    elseif love.keyboard.isDown("up") and player.y > 0 then
      if player.ySpeed < player.maxSpeed then
        player.ySpeed = -player.maxSpeed
      end
    end
    
    if love.keyboard.isDown("right") and player.x < love.graphics.getWidth() - player.w then
      if player.xSpeed < player.maxSpeed then
        player.xSpeed = player.maxSpeed
      end
    elseif love.keyboard.isDown("left") and player.x > 0 then
      if player.xSpeed < player.maxSpeed then
        player.xSpeed = -player.maxSpeed
      end
    end
  
  end
  
  player.x = player.x + player.xSpeed * dt
  player.y = player.y + player.ySpeed * dt
  
  player.xSpeed = player.xSpeed * (1 - math.min(dt * player.friction))
  player.ySpeed = player.ySpeed * (1 - math.min(dt * player.friction))
  
  if enemy_controller.enemies_killed.basic == levels[level].enemies.basic then
    -- level ended
    level_won = true
    if level_won_time >= 0 then
        level_won_time = level_won_time - dt
        return
    end
    
    local winning_speed = player.maxSpeed * dt * 200
  
    if level_won_back_time >= 0 then
      level_won_back_time = level_won_back_time - dt
      winning_speed = -(player.maxSpeed * dt * 10)
    end
    
    player.xSpeed = winning_speed
    
    if (player.x < love.graphics.getWidth()) then
      return
    end
    
    level = level + 1
    
    -- check if won the game
    if level > table.maxn(levels) then
      state = "ending"
    else
      state = "level"
    end
    
    reset_everything()
    return
  end
  
  if dieing then
    if dieing_time >= 0 then
      dieing_time = dieing_time - dt
    else
      state = "game_over"
      level = 0
      player.points = 0
      reset_everything()
    end
    
    -- update animations
    animation.currentTime = animation.currentTime + dt
    
    return
  end
  
  if love.keyboard.isDown("space") then
    player.fire()
  end
  
  -- handle fire cooldown
  if player.cooldown > 0 then
    player.cooldown = player.cooldown - 1
  end
end

function game_draw()
  -- debug
  love.graphics.setColor(255,255,255)
  --love.graphics.print("Cooldown: " .. player.cooldown)
  --love.graphics.print("Fire rate: " .. enemy_controller.fire_rate)
  --love.graphics.print("Fire cooldown: " .. enemy_controller.do_fire, 300)
  --love.graphics.print(enemy_controller.do_spawn)
  love.graphics.print(player.points)
  
  -- draw start
  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(ps_bg, 0)
  
  -- draw start text
  if started > 0 and math.floor(started) % 2 == 0 then
    love.graphics.setColor(255, 255, 255)
    local text = "Ready!"
    local textWidth = fonts.game_over:getWidth(text)
    local textHeight = fonts.game_over:getHeight(text)
    
    love.graphics.print(text, (love.graphics.getWidth()/2) - (textWidth/2), (love.graphics.getHeight()/2) - textHeight/2)
  end
  
  -- draw player
  if dieing then
    if animation.currentTime < animation.duration then
      local spriteNum = math.floor(animation.currentTime / animation.duration * #animation.quads) + 1
      love.graphics.draw(animation.spritesheet, animation.quads[spriteNum], player.x - 64, player.y - 64, 0, 4)
    end
  else
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(player_image, player.x, player.y)
  end
  
  -- draw enemy explosions
  for i, explosion in ipairs(enemy_controller.explosions) do
    if explosion.animation.currentTime < explosion.animation.duration then
      explosion.spriteNum = math.floor(explosion.animation.currentTime / explosion.animation.duration * #explosion.animation.quads) + 1
      love.graphics.draw(explosion.animation.spritesheet, explosion.animation.quads[explosion.spriteNum], explosion.x - 32, explosion.y - 32, 0, 1)
    end
  end
  
  -- draw bullets
  love.graphics.setColor(255, 255, 255)
  for _, bullet in pairs(player.bullets) do
    love.graphics.draw(bullet_image, bullet.x, bullet.y)
    love.graphics.draw(bullet.ps, 0, 5)
  end
  
  -- draw enemies
  love.graphics.setColor(255,255,255);
  for _, enemy in pairs(enemy_controller.enemies) do
    love.graphics.draw(enemy_image, enemy.x, enemy.y)
  end
  
  -- draw enemy bullets
  love.graphics.setColor(255, 80, 0)
  for _, bullet in pairs(enemy_controller.bullets) do
    love.graphics.draw(enemy_bullet_image, bullet.x, bullet.y)
  end
end

function game_keypressed(key)
  if key == "escape" then
    state = "menu"
  end
end

function newAnimation(image, width, height, duration)
  local animation = {}
  animation.spritesheet = image
  animation.quads = {}
  
  for y = 0, image:getHeight() - height, height do
    for x = 0, image:getWidth() - width, width do
      table.insert(animation.quads, love.graphics.newQuad(x, y, width, height, image:getDimensions()))
    end
  end
  
  animation.duration = duration or 1
  animation.currentTime = 0
  
  return animation
end

function move_enemy(enemy, dt)
  if enemy.type.name == 'basic' then
    -- handle movement
    if enemy.moving then
      enemy.movement_left = enemy.movement_left - 1 * dt
      enemy.y = enemy.y + (enemy.type.speed * dt) * enemy.movement_direction
      
      if enemy.movement_left <= 0 then
        enemy.moving = false
      end
      
      return
    end
    
    -- handle start moving
    local rand = math.random(100)
    
    if rand <= 1 then
      enemy.moving = true
      enemy.movement_left = 50/100
      enemy.movement_direction = math.random(0,1) == 1 and -1 or 1
    end
  elseif enemy.type.name == 'crosser' then
    if enemy.movement_direction ~= nil then
      enemy.y = enemy.y + (enemy.type.speed * dt) * enemy.movement_direction
      return
    end
    
    enemy.movement_direction = enemy.y == -30 and -1 or 1
  end
end

function reset_everything()
  -- reset everything
  level_won = false
  level_won_back_time = 1
  player.x = 20
  animation.currentTime = 0
  dieing_time = dieing_waiting_time
  dieing = false
  enemy_controller.enemies = {}
  enemy_controller.bullets = {}
  enemy_controller.enemies_killed.basic = 0
  enemy_controller.explosions = {}
  player.bullets = {}
  player.level_points = 0
  started = startTime
  music.game:stop()
  music.game_over:play()
end