function ending_load()
  fonts.ending = love.graphics.newFont("assets/Connection.otf", 60)
end

function ending_update(dt)
end

function ending_draw()
  local text = "You won!"
  love.graphics.setColor(255, 255, 255)
  love.graphics.setFont(fonts.ending)
  local fontWidth = fonts.game_over:getWidth(text)
  local fontHeight = fonts.game_over:getHeight(text)
  love.graphics.print(text, (love.graphics.getWidth() / 2) - (fontWidth/2), (love.graphics.getHeight()/2) - fontHeight/2)
end